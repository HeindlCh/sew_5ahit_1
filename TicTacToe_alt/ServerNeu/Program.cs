﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ServerNeu
{
    class Program
    {
        static StreamReader sr;
        static StreamWriter sw;

        static bool player1 = false;
        static bool player2 = true;
        static bool[] p1B;
        static bool[] p2B;

        static void Main(string[] args)
        {
            p1B = p2B = initArrays();

            TcpListener listener = new TcpListener(IPAddress.Any, 4711);
            listener.Start();

            Console.WriteLine("Server is starting...");
            TcpClient client = listener.AcceptTcpClient();

            sr = new StreamReader(client.GetStream());
            sw = new StreamWriter(client.GetStream());
            sw.AutoFlush = true;

            Thread t1 = new Thread(HandleClient);
            Thread t2 = new Thread(HandleClient);
            t1.Start();
            t2.Start();
        }

        static bool[] initArrays()
        {
            bool[] array = new bool[9];
            for (int i = 0; i < 9; i++)
            {
                array[i] = false;
            }

            return array;
        }

        static void HandleClient(Object c)
        {
            string lesen = "";
            int button;
            TcpClient client = (TcpClient)c;

            Console.WriteLine("Spiel wird gestartet...");
            sw.WriteLine("Spiel wird gestartet...");

            while (true)
            {
                lesen = sr.ReadLine();
                if (lesen != "")
                {
                    IteratePlayer();
                    if (player1)
                    {
                        Console.WriteLine("Player 1 hat Button " + lesen + " gedrückt");
                        sw.WriteLine(lesen);

                        button = Convert.ToInt32(lesen);
                        p1B[button - 1] = true;
                    }
                    if (player2)
                    {
                        Console.WriteLine("Player 2 hat Button " + lesen + " gedrückt");
                        sw.WriteLine(lesen);

                        button = Convert.ToInt32(lesen);
                        p2B[button - 1] = true;
                    }

                }
                Checkit();
            }
        }
        static void Checkit()
        {
            //SPIELER 1

            //waagrecht
            if (p1B[1] == true && p1B[2] == true && p1B[3] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            if (p1B[4] == true && p1B[5] == true && p1B[6] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            if (p1B[7] == true && p1B[8] == true && p1B[9] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            //senkrecht
            if (p1B[1] == true && p1B[4] == true && p1B[7] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            if (p1B[2] == true && p1B[5] == true && p1B[8] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            if (p1B[3] == true && p1B[6] == true && p1B[9] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            //diagonal
            if (p1B[1] == true && p1B[5] == true && p1B[9] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");

            if (p1B[3] == true && p1B[5] == true && p1B[7] == true)
                sw.WriteLine("Spieler 1 hat gewonnen");


            //SPIELER 2

            //waagrecht
            if (p2B[1] == true && p2B[2] == true && p2B[3] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            if (p2B[4] == true && p2B[5] == true && p2B[6] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            if (p2B[7] == true && p2B[8] == true && p2B[9] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            //senkrecht
            if (p2B[1] == true && p2B[4] == true && p2B[7] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            if (p2B[2] == true && p2B[5] == true && p2B[8] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            if (p2B[3] == true && p2B[6] == true && p2B[9] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            //diagonal
            if (p2B[1] == true && p2B[5] == true && p2B[9] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");

            if (p2B[3] == true && p2B[5] == true && p2B[7] == true)
                sw.WriteLine("Spieler 2 hat gewonnen");
        }
        static void IteratePlayer()
        {
            if (player1 == true)
            {
                player1 = false;
                player2 = true;

                sw.WriteLine("Spieler 1 ist dran");
            }

            else if (player2 == true)
            {
                player2 = false;
                player1 = true;

                sw.WriteLine("Spieler 2 ist dran");
            }
        }
    }
}
