﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using System.Net;

namespace Server
{
    class MainClass
    {
        static TcpListener listener;
        const int LIMIT = 5;

        public static void Main()
        {
            listener = new TcpListener(IPAddress.Any, 2055);
            listener.Start();
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamWriter sw = new StreamWriter(s);
                    StreamReader sr = new StreamReader(s);

                    sw.AutoFlush = true;
                    string request = sr.ReadLine();
                    string[] splitRequest = request.Split('|');

                    if (splitRequest[0] == "DIR")
                    {
                        try
                        {
                            Console.WriteLine(splitRequest[1]);
                            DirectoryInfo dirInfo = new DirectoryInfo(splitRequest[1]);
                            string xml = dirInfo.ToXML().ToString();

                            Console.WriteLine(xml);
                            sw.Write(xml);
                            sw.Close();
                        }
                        catch (Exception e)
                        {
                            //sw.Write("error: " + e.Message);
                        }
                    }
                    else if (splitRequest[0] == "FILE")
                    {
                        try
                        {
                            Console.WriteLine(splitRequest[1]);
                            DirectoryInfo dirInfo = new DirectoryInfo(splitRequest[1]);
                            var files = dirInfo.GetFiles();
                            XElement xml = new XElement("Directory");

                            foreach (FileInfo fi in files)
                                xml.Add(fi.ToXML());

                            sw.Write(xml);
                            sw.Close();
                        }
                        catch (Exception e)
                        {
                            //sw.Write("error: " + e.Message);
                        }
                    }
                    s.Close();
                }
                catch (Exception)
                {
                }
                soc.Close();
            }
        }
    }

    public static class Extension
    {
        public static XElement ToXML(this DirectoryInfo dir)
        {
            return new XElement("DIR",
                new XAttribute("Name", dir.Name),
                dir.GetDirectories().Select(d =>
                {
                    try { return ToXML(d); }
                    catch { return new XElement("DIR", 
                                new XAttribute("Name", d.Name)); 
                    }
                }));
        }

        public static XElement ToXML(this FileInfo file)
        {
            return new XElement("FILE",
                new XAttribute("Name", file.Name),
                new XAttribute("Length", file.Length),
                new XAttribute("LastWriteTime", file.LastWriteTime));
        }
    }
}
