﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace ServerNeu
{
    class Program
    {
        static StreamReader sr;
        static StreamWriter sw;

        static bool player1 = false;
        static bool player2 = true;
        static bool[] p1B;
        static bool[] p2B;

        static void Main(string[] args)
        {
            p1B = p2B = initArrays();

            TcpListener listener = new TcpListener(IPAddress.Any, 4711);
            listener.Start();

            Console.WriteLine("Server is starting...");
            TcpClient client = listener.AcceptTcpClient();

            sr = new StreamReader(client.GetStream());
            sw = new StreamWriter(client.GetStream());
            sw.AutoFlush = true;

            for (int i = 0; i < 2; i++)
            {
                Thread t = new Thread(HandleClient);
                t.Start();
            }
            //Thread t1 = new Thread(HandleClient);
            //Thread t2 = new Thread(HandleClient);
            //t1.Start();
            //t2.Start();
        }

        static bool[] initArrays()
        {
            bool[] array = new bool[9];
            for (int i = 0; i < 9; i++)
            {
                array[i] = false;
            }

            return array;
        }

        static void HandleClient(Object c)
        {
            string lesen = "";
            string ruckgabe;
            int button;
            TcpClient client = (TcpClient)c;
            Logik l = new Logik();

            Console.WriteLine("Spiel wird gestartet...");
            sw.WriteLine("Spiel wird gestartet...");

            while (true)
            {
                lesen = sr.ReadLine();
                Console.WriteLine(  "server liest+"+ lesen);
                if (lesen != "")
                {
                    ruckgabe = l.IteratePlayer(player1, player2);

                    if (ruckgabe == "Spieler1")
                    {
                        player1 = true;
                        player2 = false;
                    }

                    else if(ruckgabe == "Spieler2")
                    {
                        player1 = false;
                        player2 = true;
                    }



                    if (player1)
                    {
                        Console.WriteLine("Player 1 hat Button " + lesen + " gedrückt");
                        sw.WriteLine(lesen);

                        button = Convert.ToInt32(lesen);
                        p1B[button - 1] = true;
                    }
                    if (player2)
                    {
                        Console.WriteLine("Player 2 hat Button " + lesen + " gedrückt");
                        sw.WriteLine(lesen);

                        button = Convert.ToInt32(lesen);
                        p2B[button - 1] = true;
                    }

                }
                sw.WriteLine(l.Checkit(p1B, p2B));
            }
        }
    }
}
