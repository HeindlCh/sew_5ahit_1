﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace Server
{
    public partial class Server : Form
    {
        StreamReader sr;
        StreamWriter sw;

        public Server()
        {
            InitializeComponent();
        }

        private void Server_Load(object sender, EventArgs e)
        {
            string lesen = "";
            List<string> pushed = new List<string>();

            TcpListener listener = new TcpListener(IPAddress.Any, 4711);
            listener.Start();

            MessageBox.Show("Server is starting...");
            TcpClient client = listener.AcceptTcpClient();

            sr = new StreamReader(client.GetStream());
            sw = new StreamWriter(client.GetStream());
            sw.AutoFlush = true;

            Thread t1 = new Thread(HandleClient1);
            Thread t2 = new Thread(HandleClient2);
            t1.Start();
            t2.Start();     

            while(true)
            {
                lesen = sr.ReadLine();
                if (lesen != "")
                {
                    MessageBox.Show(lesen);
                    pushed.Add(lesen);
                }
            }
        }

        private void HandleClient1(Object c)
        {
            TcpClient client = (TcpClient)c;

            sw.WriteLine("You are player 1");
        }

        private void HandleClient2(Object c)
        {
            TcpClient client = (TcpClient)c;

            sw.WriteLine("You are player 2");
        }
    }
}
