﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerNeu
{
    class Logik
    {

        public string Checkit(bool[] p1B, bool[] p2B)
        {
            //SPIELER 1

            //waagrecht
            if (p1B[1] == true && p1B[2] == true && p1B[3] == true)
                return("Spieler 1 hat gewonnen");

            else if (p1B[4] == true && p1B[5] == true && p1B[6] == true)
                return ("Spieler 1 hat gewonnen");

            else if (p1B[7] == true && p1B[8] == true && p1B[9] == true)
                return ("Spieler 1 hat gewonnen");

            //senkrecht
            else if (p1B[1] == true && p1B[4] == true && p1B[7] == true)
                return ("Spieler 1 hat gewonnen");

            else if (p1B[2] == true && p1B[5] == true && p1B[8] == true)
                return ("Spieler 1 hat gewonnen");

            else if (p1B[3] == true && p1B[6] == true && p1B[9] == true)
                return ("Spieler 1 hat gewonnen");

            //diagonal
            else if (p1B[1] == true && p1B[5] == true && p1B[9] == true)
                return ("Spieler 1 hat gewonnen");

            else if (p1B[3] == true && p1B[5] == true && p1B[7] == true)
                return ("Spieler 1 hat gewonnen");


            //SPIELER 2

            //waagrecht
            if (p2B[1] == true && p2B[2] == true && p2B[3] == true)
                return ("Spieler 2 hat gewonnen");

            else if (p2B[4] == true && p2B[5] == true && p2B[6] == true)
                return ("Spieler 2 hat gewonnen");

            else if (p2B[7] == true && p2B[8] == true && p2B[9] == true)
                return ("Spieler 2 hat gewonnen");

            //senkrecht
            else if (p2B[1] == true && p2B[4] == true && p2B[7] == true)
                return ("Spieler 2 hat gewonnen");

            else if (p2B[2] == true && p2B[5] == true && p2B[8] == true)
                return ("Spieler 2 hat gewonnen");

            else if (p2B[3] == true && p2B[6] == true && p2B[9] == true)
                return ("Spieler 2 hat gewonnen");

            //diagonal
            else if (p2B[1] == true && p2B[5] == true && p2B[9] == true)
                return ("Spieler 2 hat gewonnen");

            else if (p2B[3] == true && p2B[5] == true && p2B[7] == true)
                return ("Spieler 2 hat gewonnen");

            else
                return "Noch kein Sieger";
        }
        public string IteratePlayer(bool player1, bool player2)
        {
            if (player1 == true)
            {
                player1 = false;
                player2 = true;

                return ("Spieler1");
            }

            else if (player2 == true)
            {
                player2 = false;
                player1 = true;

                return ("Spieler2");
            }

            else
                return "";
        }
    }
}
