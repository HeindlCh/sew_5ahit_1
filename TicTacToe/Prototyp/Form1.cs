﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Prototyp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        int turn = 1;
        int winner = 0;
        int player1 = 0;
        int player2 = 0;

        public void display()
        {
            if (turn % 2 != 0)
            {
                displayTurn.Text = "Player 1";
            }

            else
            {
                displayTurn.Text = "Player 2";
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            displayTurn.Text = "Player 1";
            player1score.Text = "0";
            player2score.Text = "0";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        public void checkit()
        {
            bool player1Win = false;
            bool player2Win = false;

            if (button1.Text != "" & button2.Text != "" & button3.Text != "")
            {
                if (button1.Text == button2.Text && button1.Text == button3.Text)
                {
                    button1.BackColor = button2.BackColor = button3.BackColor = Color.Green;
                    button1.ForeColor = button2.ForeColor = button3.ForeColor = Color.White;

                    if (button1.Text == "X")
                    {
                        player1Win = true;

                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (button4.Text != "" & button5.Text != "" & button6.Text != "")
            {
                if (button4.Text == button5.Text && button4.Text == button6.Text)
                {
                    button4.BackColor = button5.BackColor = button6.BackColor = Color.Green;
                    button4.ForeColor = button5.ForeColor = button6.ForeColor = Color.White;

                    if (button4.Text == "X")
                    {
                        player1Win = true;
                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (button7.Text != "" & button8.Text != "" & button9.Text != "")
            {
                if (button7.Text == button8.Text && button7.Text == button9.Text)
                {
                    button7.BackColor = button8.BackColor = button9.BackColor = Color.Green;
                    button7.ForeColor = button8.ForeColor = button9.ForeColor = Color.White;

                    if (button7.Text == "X")
                    {
                        player1Win = true;
                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (button1.Text != "" & button4.Text != "" & button7.Text != "")
            {
                if (button1.Text == button4.Text && button1.Text == button7.Text)
                {
                    button1.BackColor = button4.BackColor = button7.BackColor = Color.Green;
                    button1.ForeColor = button4.ForeColor = button7.ForeColor = Color.White;

                    if (button1.Text == "X")
                    {
                        player1Win = true;
                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }


            if (button2.Text != "" & button5.Text != "" & button8.Text != "")
            {
                if (button2.Text == button5.Text && button2.Text == button8.Text)
                {
                    button2.BackColor = button5.BackColor = button8.BackColor = Color.Green;
                    button2.ForeColor = button5.ForeColor = button8.ForeColor = Color.White;

                    if (button2.Text == "X")
                    {
                        player1Win = true;
                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (button3.Text != "" & button6.Text != "" & button9.Text != "")
            {
                if (button3.Text == button6.Text && button3.Text == button9.Text)
                {
                    button3.BackColor = button6.BackColor = button9.BackColor = Color.Green;
                    button3.ForeColor = button6.ForeColor = button9.ForeColor = Color.White;

                    if (button3.Text == "X")
                    {
                        player1Win = true;
                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (button1.Text != "" & button5.Text != "" & button9.Text != "")
            {
                if (button1.Text == button5.Text && button1.Text == button9.Text)
                {
                    button1.BackColor = button5.BackColor = button9.BackColor = Color.Green;
                    button1.ForeColor = button5.ForeColor = button9.ForeColor = Color.White;

                    if (button1.Text == "X")
                    {
                        player1Win = true;
                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (button3.Text != "" & button5.Text != "" & button7.Text != "")
            {
                if (button3.Text == button5.Text && button3.Text == button7.Text)
                {
                    button3.BackColor = button5.BackColor = button7.BackColor = Color.Green;
                    button3.ForeColor = button5.ForeColor = button7.ForeColor = Color.White;

                    if (button3.Text == "X")
                    {
                        player1Win = true;

                    }
                    else
                    {
                        player2Win = true;
                    }
                }
            }

            if (player1Win)
            {
                MessageBox.Show("Player 1 wins!");
                player1Win = false;
                winner = 1;
            }

            else if (player2Win)
            {
                MessageBox.Show("Player 2 wins!");
                player2Win = false;
                winner = 2;
            }

            count(winner);
        }

        private void count(int winnerC)
        {
            if (winnerC == 1)
                player1++;

            else if (winnerC == 2)
                player2++;
             

            player1score.Text = player1.ToString();
            player2score.Text = player2.ToString();

            winner = 0;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button1.Text = "X";

                    turn++;
                }
                else
                {
                    button1.Text = "O";

                    turn--;
                }
            }
            else
            {
                button1.Text = button1.Text;
            }

            display();
            checkit();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (button2.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button2.Text = "X";

                    turn++;
                }
                else
                {
                    button2.Text = "O";

                    turn--;
                }
            }
            else
            {
                button2.Text = button2.Text;
            }

            display();
            checkit();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (button3.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button3.Text = "X";

                    turn++;
                }
                else
                {
                    button3.Text = "O";

                    turn--;
                }
            }
            else
            {
                button3.Text = button3.Text;
            }

            display();
            checkit();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (button4.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button4.Text = "X";

                    turn++;
                }
                else
                {
                    button4.Text = "O";

                    turn--;
                }
            }
            else
            {
                button4.Text = button4.Text;
            }

            display();
            checkit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (button5.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button5.Text = "X";

                    turn++;
                }
                else
                {
                    button5.Text = "O";

                    turn--;
                }
            }
            else
            {
                button5.Text = button5.Text;
            }

            display();
            checkit();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (button6.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button6.Text = "X";

                    turn++;
                }
                else
                {
                    button6.Text = "O";

                    turn--;
                }
            }
            else
            {
                button6.Text = button6.Text;
            }

            display();
            checkit();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (button7.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button7.Text = "X";

                    turn++;
                }
                else
                {
                    button7.Text = "O";

                    turn--;
                }
            }
            else
            {
                button7.Text = button7.Text;
            }

            display();
            checkit();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (button8.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button8.Text = "X";

                    turn++;
                }
                else
                {
                    button8.Text = "O";

                    turn--;
                }
            }
            else
            {
                button8.Text = button8.Text;
            }

            display();
            checkit();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (button9.Text == "")
            {
                if (turn % 2 != 0)
                {
                    button9.Text = "X";

                    turn++;
                }
                else
                {
                    button9.Text = "O";

                    turn--;
                }
            }
            else
            {
                button9.Text = button9.Text;
            }

            display();
            checkit();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            button1.BackColor = button2.BackColor = button3.BackColor = button4.BackColor = button5.BackColor = button6.BackColor = button7.BackColor = button8.BackColor = button9.BackColor = Color.Empty;
            button1.Text = button2.Text = button3.Text = button4.Text = button5.Text = button6.Text = button7.Text = button8.Text = button9.Text = "";
            button1.ForeColor = button2.ForeColor = button3.ForeColor = button4.ForeColor = button5.ForeColor = button6.ForeColor = button7.ForeColor = button8.ForeColor = button9.ForeColor = Color.Empty;
        }

        private void button11_Click(object sender, EventArgs e)
        {
            button1.Enabled = button2.Enabled = button3.Enabled = button4.Enabled = button5.Enabled = button6.Enabled = button7.Enabled = button8.Enabled = button9.Enabled = true;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Game will close");
            this.Close();
        }
    }
}
